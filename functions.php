<?php
// Load child stylesheet

add_action('wp_enqueue_scripts', 'dlist_child_theme_styles');
function dlist_child_theme_styles()
{

	if (is_rtl()) {
		wp_enqueue_style('child-style', get_stylesheet_directory_uri() . '/style.css', array('dlist-style'), null);
	} else {
		wp_enqueue_style('parent-style', get_parent_theme_file_uri('/style.css'));
	}
}

// MAKE FIELD KEY VISIBLE

add_filter('directorist_custom_field_meta_key_field_args', function ($args) {
	$args['type'] = 'text';
	return $args;
});

// MEasurment Unit Text

if( !function_exists('measurment_unit_text') ){
	function measurment_unit_text( $unit = 0, $measurment_unit = 'piece' )
	{
		if( $unit == 0 ) return '';
		
		$s = $unit > 1 ? false : true;
		$text = 'Piece';
		switch( $measurment_unit )
		{
			case 'piece':
				$text = $s ? 'Piece' : 'Pieces';
			break;
			case 'dozen':
				$text = $s ? 'Dozen' : 'Dozens';
			break;
			case 'kg':
				$text = $s ? 'KG' : 'KG';
			break;
			case 'liter':
				$text = $s ? 'Liter' : 'Liters';
			break;
			default:
				$text = $measurment_unit;
			break;
		}
		return $text;
	}
}

//Auto click on filter button
/*
add_action('wp_footer', function(){
?>
	<script>
		jQuery(document).ready(function(){
			jQuery('.directorist-filter-btn').trigger('click')
		})
	</script>
<?php
});
*/