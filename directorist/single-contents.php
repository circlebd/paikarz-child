<?php
/**
 * @author  wpWax
 * @since   6.7
 * @version 6.7
 */

use \Directorist\Directorist_Single_Listing;
use \Directorist\Helper;

if ( ! defined( 'ABSPATH' ) ) exit;

$listing = Directorist_Single_Listing::instance();
?>

<div class="directorist-single-contents-area">

	<?php $listing->notice_template(); ?>

	<div class="<?php Helper::directorist_row(); ?>">

		<div class="<?php Helper::directorist_single_column(); ?>">

			<div class="directorist-single-wrapper">
				<?php if ( $listing->single_page_enabled() ): ?>

					<div class="directorist-single-wrapper">

						<?php echo $listing->single_page_content(); ?>

					</div>

				<?php else: ?>
				
					<?php
					$listing->header_template();

					foreach ( $listing->content_data as $section ) {
                        if( isset($section['custom_block_id']) && $section['custom_block_id'] == 'contact-information' ){
                            $hide_contact = get_post_meta( $listing->id, '_hide_contact_information', true ) ? get_post_meta( $listing->id, '_hide_contact_information', true ) : array();
                            if( !in_array( 'hide', $hide_contact ) ){
                                $listing->section_template( $section );
                            }
                        }else{
						    $listing->section_template( $section );
                        }
					}
					?>
				<?php endif; ?>
			</div>

		</div>

		<?php Helper::get_template( 'single-sidebar' ); ?>

	</div>

</div>