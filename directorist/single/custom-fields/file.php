<?php
/**
 * @author  wpWax
 * @since   6.6
 * @version 7.0.5.2
 */

if ( ! defined( 'ABSPATH' ) ) exit;

$done = str_replace( '|||', '', $value );
$name_arr = explode( '/', $done );
$filename = end( $name_arr );
?>

<div class="directorist-single-info directorist-single-info-file">

    <?php if (isset($data['field_key']) && $data['field_key'] == 'featured-video' && !empty( $done )) : ?>
        <video width="100%" controls>
            <source src="<?php echo esc_url( $done  ); ?>" type="video/mp4">
            Your browser does not support the video tag.
        </video>
    <?php else: ?>
	<div class="directorist-single-info__label">
		<span class="directorist-single-info__label-icon"><?php directorist_icon( $icon );?></span>
		<span class="directorist-single-info__label--text"><?php echo esc_html( $data['label'] ); ?></span>
	</div>
	
	<div class="directorist-single-info__value"><?php printf('<a href="%s" target="_blank" download>%s</a>', esc_url( $done ), $filename ); ?></div>
	
    <?php endif; ?>
</div>