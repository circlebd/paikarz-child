<?php
/**
 * @author  wpWax
 * @since   6.7
 * @version 7.0.5.2
 */

if ( ! defined( 'ABSPATH' ) ) exit;
?>

<div class="directorist-single-info directorist-single-info-number">

	<div class="directorist-single-info__label">
		<span class="directorist-single-info__label-icon"><?php directorist_icon( $icon );?></span>
		<span class="directorist-single-info__label--text"><?php echo esc_html( $data['label'] ); ?></span>
	</div>
	
    <?php if (isset($data['field_key']) && $data['field_key'] !== 'booking-shortcode') : ?>
    <?php
        $measurement_unit = get_post_meta($listing->id, '_measurement_unit', true);   
    ?>
	<div class="directorist-single-info__value"><?php echo esc_html( $value ). ' ' .measurment_unit_text($value, $measurement_unit); ?></div>
    <?php endif; ?>
	
</div>