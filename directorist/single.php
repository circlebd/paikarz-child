<?php
/**
 * @author  wpWax
 * @since   6.6
 * @version 6.7
 */

use \Directorist\Helper;

if ( ! defined( 'ABSPATH' ) ) exit;

get_header( 'directorist' );

global $post;
$listing_id            = $post->ID;
$listing_author_id     = get_post_field( 'post_author', $post->ID );
$tagline               = get_post_meta( get_the_ID(), '_tagline', true );
$enable_new_listing    = get_directorist_option( 'display_new_badge_cart', 1 );
$popular_badge_text    = get_directorist_option( 'popular_badge_text', 'Popular' );
$feature_badge_text    = get_directorist_option( 'feature_badge_text', 'Feature' );
$new_badge_text        = get_directorist_option( 'new_badge_text', 'New' );
$display_tagline_field = get_directorist_option( 'display_tagline_field', 0 );
$enable_social_share   = get_directorist_option( 'enable_social_share', 1 );
$enable_favourite      = get_directorist_option( 'enable_favourite', 1 );
$enable_report_abuse   = get_directorist_option( 'enable_report_abuse', 1 );
$is_disable_price      = get_directorist_option( 'disable_list_price' );
$display_pricing_field = get_directorist_option( 'display_pricing_field', 1 );
$enable_review         = get_directorist_option( 'enable_review', 'yes' );
$reviews_count         = ATBDP()->review->db->count( array( 'post_id' => $listing_id ) );

$display_feature_badge_single         = get_directorist_option( 'display_feature_badge_cart', 1 );
$display_popular_badge_single         = get_directorist_option( 'display_popular_badge_cart', 1 );
$listing_info['featured']             = get_post_meta( $post->ID, '_featured', true );
$listing_info['tagline']              = get_post_meta( $post->ID, '_tagline', true );
$listing_info['price']                = get_post_meta( $post->ID, '_price', true );
$listing_info['price_range']          = get_post_meta( $post->ID, '_price_range', true );
$listing_info['atbd_listing_pricing'] = get_post_meta( $post->ID, '_atbd_listing_pricing', true );
extract( $listing_info );
?>

<section class="listing-details-wrapper bgimage">
	<?php dlist_single_listing_header_background(); ?>
	<div class="container content_above">
		<div class="row">
			<div class="col-lg-12">
				<div class="listing_action_btns">

					<div class="edit_btn_wrap">
						<?php
						if ( is_user_logged_in() && $listing_author_id == get_current_user_id() ) {
							$payment   = isset( $_GET['payment'] ) ? $_GET['payment'] : '';
							$url       = isset( $_GET['redirect'] ) ? $_GET['redirect'] : '';
							$edit_link = ! empty( $payment ) ? add_query_arg( 'redirect', $url, ATBDP_Permalink::get_edit_listing_page_link( $post->ID ) ) : ATBDP_Permalink::get_edit_listing_page_link( $post->ID );

							if ( isset( $_GET['redirect'] ) ) {
								?>
								<div class="atbdp_float_active">
									<?php echo atbdp_get_preview_button(); ?>
									<a href="<?php echo esc_url( $edit_link ); ?>"
									   class="edit-listing-btn">
										<span class="<?php atbdp_icon_type( true ); ?>-edit"></span>
										<?php esc_html_e( 'Edit Listing', 'dlist' ); ?>
									</a>
								</div>
								<?php
							} else {
								?>
								<a href="javascript:history.back()" class="atbd_go_back">
									<i class="<?php atbdp_icon_type( true ); ?>-long-arrow-left"></i>
									<?php esc_html_e( 'Go Back', 'dlist' ); ?>
								</a>
								<a href="<?php echo esc_url( $edit_link ); ?>"
								   class="edit-listing-btn">
									<span class="<?php atbdp_icon_type( true ); ?>-edit"></span>
									<?php esc_html_e( 'Edit Listing', 'dlist' ); ?>
								</a>
								<?php
							}
						} else {
							?>
							<a href="javascript:history.back()" class="atbd_go_back">
								<i class="<?php atbdp_icon_type( true ); ?>-long-arrow-left"></i>
								<?php esc_html_e( 'Go Back', 'dlist' ); ?>
							</a>
							<?php
						}
						?>
					</div>

					<div class="atbd_listing_action_area">
						<?php do_action( 'dlist_listing_detail_before_favourite' ); ?>
						<?php if ( $enable_favourite ) { ?>
							<div class="atbd_action atbd_save atbd_tooltip" id="atbdp-favourites" aria-label="Favorite">
								<?php echo the_atbdp_favourites_link(); ?>
							</div>
							<?php
						}
						if ( $enable_social_share ) {
							?>
							<div class="atbd_action atbd_share dropdown atbd_tooltip" aria-label="Share">
								<?php dlist_sharing(); ?>
							</div>
							<?php
						}
						if ( $enable_report_abuse ) {
							get_template_part('directorist/single/fields/report');
						} ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="listing-details-contents">
	<div class="listing-info">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-7">
					<div class="dlist_single_listing_title">

						<div class="single-listing-title">
							<?php the_title( '<h2>', '</h2>' ); ?>

							<?php if ( $enable_new_listing || $display_feature_badge_single || $display_popular_badge_single ) { ?>
								<ul class="list-unstyled listing-info--badges">
									<?php
									$popular_listing_id = atbdp_popular_listings( get_the_ID() );

									if ( ! empty( new_badge() ) ) {
										echo sprintf( '<li>%s</li>', new_badge() );
									}
									if ( $featured && ! empty( $display_feature_badge_single ) ) {
										printf( '<li><span class="atbd_badge atbd_badge_featured">%s</span></li>', esc_attr( $feature_badge_text ) );
									}

									if ( ( $popular_listing_id === get_the_ID() ) && $display_popular_badge_single ) {
										printf( '<li><span class="atbd_badge atbd_badge_popular">%s</span></li>', esc_attr( $popular_badge_text ) );
									}
									?>
								</ul>
							<?php } ?>

							<?php do_action( 'directorist_single_listing_after_title', get_the_ID() ); ?>
							
						</div>

						<?php if ( $tagline && $display_tagline_field ) { ?>
							<p class="atbd_sub_title subtitle"><?php echo ! empty( $tagline ) ? esc_html( stripslashes( $tagline ) ) : ''; ?></p>
						<?php } ?>

					</div>

					<ul class="list-unstyled listing-info--meta">
						<?php
						$atbd_listing_pricing = ! empty( $atbd_listing_pricing ) ? $atbd_listing_pricing : '';

						if ( ! $is_disable_price && $display_pricing_field ) {
							if ( $price_range && ( 'range' === $atbd_listing_pricing ) ) {
								echo atbdp_display_price_range( $price_range );
							} else {
								if ( $price ) {
                                    $unit = get_post_meta($listing_id, '_unit', true) ? get_post_meta($listing_id, '_unit', true): '';
                                    $measurement_unit = get_post_meta($listing_id, '_measurement_unit', true) ? get_post_meta($listing_id, '_measurement_unit', true): '';
									echo wp_kses_post( '<li class="single-custom-price">' );
									atbdp_display_price( $price, $is_disable_price );
                                    echo !empty($unit) && !empty($measurement_unit) ? '<span class="single-price-unit"><strong>'.$unit.'</strong>'.measurment_unit_text($unit, $measurement_unit).'</span>' : '';
									echo wp_kses_post( '</li>' );
								}
							}
						}
						if ( $enable_review ) {
							?>
							<li>
								<div class="average-ratings">
									<?php dlist_listing_review(); ?>
									<span>
										<strong><?php echo wp_kses_post( $reviews_count ); ?></strong>
										<?php echo ( 1 < $reviews_count ) ? esc_html__( ' Reviews', 'dlist' ) : esc_html__( ' Review', 'dlist' ); ?>
									</span>
								</div>
							</li>
							<?php
						}

						$categories = get_the_terms( get_the_ID(), ATBDP_CATEGORY );
						
						if ( $categories ) {
							echo wp_kses_post( '<li><ul>' );
							foreach ( $categories as $category ) {
								$category_icon = ! empty( $category ) ? get_cat_icon( $category->term_id ) : atbdp_icon_type() . '-tags';
								$icon_type     = substr( $category_icon, 0, 2 );
								$icon          = 'la' === $icon_type ? $icon_type . ' ' . $category_icon : 'fa ' . $category_icon;

								echo sprintf( '<li><span class="%s dservice-cats-icon"></span> <a href="%s">%s </a></li>', esc_attr( $icon ), esc_url( ATBDP_Permalink::atbdp_get_category_page( $category ) ), esc_attr( $category->name ) );
							}
							echo wp_kses_post( '</ul></li>' );
						}
						?>
					</ul>
				</div>

				<?php if ( $enable_review ) { ?>
					<div class="col-lg-4 col-md-5 d-flex justify-content-start justify-content-md-end justify-content-lg-end">
						<div class="atbd_listing_action_area">
							<?php
							$review_btn  = sprintf( '<a href="%s" class="review_btn"><i class="%s-star-o"></i> %s</a>', '#directorist-review-block', atbdp_icon_type(), esc_html__( 'Add A Review', 'dlist' ) );
							$plan_review = true;
							if ( is_fee_manager_active() ) {
								$plan_review = is_plan_allowed_listing_review( get_post_meta( $post->ID, '_fm_plans', true ) );
							}
							if ( $plan_review ) {
								echo wp_kses_post( $review_btn );
							}
							?>
						</div>
					</div>
				<?php } ?>

			</div>
		</div>
	</div>
</section>

<section class="directory_listing_detail_area single_area section-padding-strict">
	<div class="container">
		<?php Helper::get_template( 'single-contents' ); ?>
	</div>
</section>

<?php
get_footer( 'directorist' );