<?php
/**
 * @author  wpWax
 * @since   6.6
 * @version 6.7
 */

if ( ! defined( 'ABSPATH' ) ) exit;

use \Directorist\Helper;
global $post;
$id = get_the_ID();

if( $data['original_field']['field_key'] == 'min_order_unit' ){
    $price = get_post_meta($id, '_price', true);
    $price = Helper::formatted_price($price);
    $unit = get_post_meta($id, '_unit', true);
    $measurement_unit = get_post_meta($id, '_measurement_unit', true);
    $price_text = $price.' <span class="light-text">( '.$unit.' '.measurment_unit_text($unit, $measurement_unit).' )</span>';
    $min_unit_text = esc_html( $value ). ' ' .measurment_unit_text($value, $measurement_unit). '<span class="light-text">( Min Order Qty )</span>';
?>
    <div class="directorist-listing-card-text"><i class="directorist-icon las la-money-bill-wave"></i><div class="directorist-listing-card-text"><?php echo $price_text; ?></div></div>
    <div class="directorist-listing-card-text"><i class="directorist-icon las la-box"></i><div class="directorist-listing-card-text"><?php echo $min_unit_text; ?></div></div>
<?php
}else{
?>
    <div class="directorist-listing-card-number"><?php directorist_icon( $icon ); ?><?php $listings->print_label( $label ); ?><?php echo esc_html( $value ); ?></div>
<?php
}

?>